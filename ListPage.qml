/*
 *   Copyright 2018 Fabian Riethmayer
 *   Copyright 2019 Emmanuel Lepage <emmanuel.lepage@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.6
import QtQuick.Controls 2.2 as Controls
import org.kde.kirigami 2.4 as Kirigami

Kirigami.ScrollablePage {
    property alias currentIndex: list.currentIndex;
    property alias model: list.model

    Kirigami.Theme.colorSet: Kirigami.Theme.View
    title: "Address book"

    background: Rectangle {
        color: Kirigami.Theme.backgroundColor
    }

    titleDelegate: Item {
        clip: true
        id: header
        implicitHeight: parent.parent.height - 2*Kirigami.Units.largeSpacing
        implicitWidth: 10

        Controls.TextField {
            id: searchField
            placeholderText: "Search"
            anchors.centerIn: parent
            anchors.margins: Kirigami.Units.largeSpacing
            width: parent.width - 2 * Kirigami.Units.largeSpacing
        }
    }

    List {
        id: list
        width: parent.width
        height: parent.height
    }

    actions {
        main: actionCollection.newContact
    }
}
